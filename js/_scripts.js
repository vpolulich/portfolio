$(document).ready(function() {
    (function($) {
        // ===== Preloader ===== //
        $(window).load(function() {
            $("#status").fadeOut();
            $("#preloader").delay(350).fadeOut(200);
            $('body').delay(350);
        });
    })(jQuery);

    // ===== Tooltip ===== //
    $('[data-toggle="tooltip"]').tooltip();

    // ===== jQuery isotope ===== //
    if ($('.isotope').length > 0) {
        var adding = 0;
        var $container = $('.isotope').isotope({
            itemSelector: '.element-item',
            layoutMode: 'fitRows',
            transitionDuration: '0.6s'
        });
        $('.js-radio-button-group a').on('click', function(e) {
            e.preventDefault();
            if ($(this).closest('li').hasClass('is-checked')) {
                return false;
            } else {
                $(this).closest('li').addClass('is-checked').siblings('li').removeClass('is-checked');
            }
            var $this = $(this);
            var filterValue = $this.data('target');
            // set filter for Isotope
            $container.isotope({filter: filterValue});
            return $(this);
        });
        $('#works').find('.addItems').on('click', function(e) {
            e.preventDefault();
            var $this = $(this);
            for (var i = 0; i < 3; i++) {
                $.get("workitems.html", function(data, status) {
                    var lists, numb, target = $('.button-group li.is-checked a').data('target');
                    lists = (target != '*') ? $(data).find('li' + target) : $(data).find('li');
                    if (lists.length > 0) {
                        numb = Math.floor(Math.random() * lists.length);
                        $container.isotope('insert', lists.eq(numb));

                        adding++;
                        (adding == 9) ? $this.remove() : "";
                    }

                });
            }
        });
    }

    // ===== jQuery Sidebar Navigation ===== //
    var sides = ["left", "right"];

    // Defines Menu Position
    if ($("#main-menu").hasClass("right")) {
        $(".nav-btn").attr("data-side", "right");
        $('.sidebar-nav').addClass("right");
    } else {
        $(".nav-btn").attr("data-side", "left");
        $('.sidebar-nav').addClass("left");
    }

    // Initialize sidebars
    for (var i = 0; i < sides.length; ++i) {
        var cSide = sides[i];
        $(".sidebar-nav." + cSide).sidebar({
            side: cSide
        });
    }

    // Click handlers
    $(".nav-btn[data-action]").on("click", function() {
        var $this = $(this);
        var action = $this.attr("data-action");
        var side = $this.attr("data-side");
        $(".sidebar-nav." + side).trigger("sidebar:" + action);
        return false;
    });

    $(".nav-menu li a").on("click", function() {
        $(".sidebar-nav").trigger("sidebar:close");
        $('.dropdown-menu ul').hide(300);
    });

    // Fixed Menu Overflow on iOS
    if (navigator.userAgent.match(/iPhone/i) ||
            navigator.userAgent.match(/iPad/i) ||
            navigator.userAgent.match(/iPod/i)
            ) {
        $('#main-menu .nav-menu').css({"margin-bottom": "100px"});
    }
    ;
    var sections = $("section");
    var navigation_links = $(".header-nav li a");
    navigation_links.on('click', function(e) {
        e.preventDefault();
    });
    sections.waypoint({
        handler: function(event, direction) {
            var active_section;
            active_section = $(this);
            if (direction === "up")
                active_section = active_section.prev();

            var active_link = $('.header-nav a[data-href="#' + active_section.attr("id") + '"]');
            navigation_links.removeClass("selected");
            active_link.addClass("selected");

        },
        offset: '25%'
    })
    navigation_links.click(function(event) {
        $.scrollTo(
                $(this).attr("data-href"),
                {
                    duration: 700,
                    offset: {'left': 0, 'top': -0.0000000001 * $(window).height()}
                }
        );
    });

    // ===== Experience Settings ===== //

    $('#experience').waypoint({
        handler: function(event, direction) {
            $(this).find('.skills-bar .fill-skills').each(function() {
                var width = $(this).data('width');
                $(this).css('width', width);
            });
        },
        offset: '60%'
    });

    // ===== jQuery niceScroll ===== //
    $("html").niceScroll({
        cursorcolor: "#333",
        cursoropacitymin: 0.7,
        cursorwidth: 10
    });

    // ===== slick slider ===== //
    $('.slick-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 2
    });
    $('.blog-slider').slick({
        dots: true,
        slidesToShow: 1,
        arrows: false,
        autoplay: true
    });
    $('.single-blog-slider').slick({
        dots: false,
        slidesToShow: 1,
        arrows: true,
        autoplay: true
    });

    // ===== Contact Form ===== //
    $('.contacts-form form').submit(function(event) {
        $(this).find('.form-group').removeClass('has-error');
        $(this).find('input[type=text], textarea').keydown(function() {
            $(this).closest('.form-group').removeClass('has-error').find('.alert').remove();
        });
        $(this).find('.alert-success, .alert-error').remove();
        var formData = {
            'name': $('input[name=name]').val(),
            'email': $('input[name=email]').val(),
            'comment': $('textarea[name=comment]').val()
        };
        // process the form
        $.ajax({
            type: 'POST',
            url: '../process.php',
            data: formData,
            dataType: 'json',
            encode: true
        })
                .done(function(data) {
                    if (!data.success) {
                        if (data.errors.name) {
                            $('#name-group').addClass('has-error');
                            $('#name-group').append('<div class="alert alert-error animated fadeIn">' + data.errors.name + '</div>');
                        }
                        if (data.errors.email) {
                            $('#email-group').addClass('has-error');
                            $('#email-group').append('<div class="alert alert-error animated fadeIn">' + data.errors.email + '</div>');

                        }
                        if (data.errors.comment) {
                            $('#message-group').addClass('has-error');
                            $('#message-group').append('<div class="alert alert-error animated fadeIn">' + data.errors.comment + '</div>');
                        }
                    } else {
                        $('.contacts-form form textarea').after('<div class="alert alert-success animated fadeIn">' + data.message + '</div>');
                        $('.contacts-form form').find('input[type=text], textarea').val('');
                    }
                });
        event.preventDefault();
    });

    // ===== Google map ===== //
    var mapStyles = [
        {"featureType": "road.highway", "stylers": [
                {"saturation": -80},
                {"visibility": "simplified"}
            ]
        },
        {"featureType": "road.arterial", "stylers": [
                {"saturation": -80},
                {"lightness": 20},
                {"visibility": "on"}
            ]
        },
        {"featureType": "road.local", "stylers": [
                {"saturation": -800},
                {"lightness": 30},
                {"visibility": "on"}
            ]
        },
        {"featureType": "poi", "stylers": [
                {"visibility": "simplified"}
            ]
        },
        {"featureType": "transit", "stylers": [
                {"saturation": -800},
                {"visibility": "simplified"}
            ]
        },
        {"featureType": "administrative.province", "stylers": [
                {"visibility": "off"}
            ]
        },
        {"featureType": "water",
            "elementType": "labels", "stylers": [
                {"visibility": "on"},
                {"lightness": -0},
                {"saturation": -0}
            ]
        },
        {"featureType": "water",
            "elementType": "geometry", "stylers": [
                {"hue": "#00baff"},
                {"lightness": -20},
                {"saturation": -75}
            ]
        }
    ];
    var $mapCanvas = $('#map_canvas'), draggableOp;
    draggableOp = true;
    if ($mapCanvas.length > 0) {
        var map = new GMaps({
            div: '#map_canvas',
            lat: -34.397,
            lng: 150.644,
            scrollwheel: false,
            draggable: draggableOp,
            zoom: 6,
            disableDefaultUI: true,
            styles: mapStyles
        });
        map.addMarker({
            lat: -34.397,
            lng: 150.644,
            icon: 'images/marker.png',
            infoWindow: {
                content: '<p>Wollongong</p>'
            }
        });
    }



    $('.photo-block').hover(function() {
        $(this).find('.icon-fighter-jet').fadeIn();
    });
    var about_li = $('.design_works > li');
    about_li.each(function() {
        var thisPH = $(this).find('.photo-block').height();
        $(this).find('.description').css('height', thisPH);
    });
    function random(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    ;
    $('ul > li').each(function() {
        var emerge = $(this).find('.emerge');
        $(emerge).attr('data-hold', random(500, 900))
    });
    (function($) {
        new TiltSlider(document.getElementById('slideshow'));
        window.setInterval(function() {
            $('nav>.current').next().trigger('click');
            if ($('nav > .current').next().index() == '-1') {
                $('nav > span').trigger('click');
            }
        }, 6000);
    })(jQuery);


    // ===== Wow init ===== //
    new WOW({
        offset: 100,
        mobile: false
    }).init();
});




