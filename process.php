<?php

$errors = array();
$data = array();
if (empty($_POST['name']))
    $errors['name'] = 'Name is required.';
if (!filter_var(($_POST['email']), FILTER_VALIDATE_EMAIL))
    $errors['email'] = 'Email is required.';
if (iconv_strlen($_POST['comment'], 'UTF-8') < 15)
    $errors['comment'] = 'Your message should have at least 15 characters.';
if (!empty($errors)) {
    $data['success'] = false;
    $data['errors'] = $errors;
} else {
    $data['success'] = true;
    $data['message'] = 'Your message was sent, thank you!';
}
echo json_encode($data);
